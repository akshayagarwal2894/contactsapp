import os
import dataset
from sqlite3 import dbapi2 as sqlite3
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash
from flask.ext.login import LoginManager, UserMixin, login_required
import redis


# create our little application :)
app = Flask(__name__)
login_manager = LoginManager()
login_manager.init_app(app)

class User(UserMixin):
    # proxy for a database of users
    db = dataset.connect('sqlite:///users.db')
    table=db['registered_users']

    def __init__(self, username, password):
        self.id = username
        self.password = password

    @classmethod
    def get(cls,id):
        return cls.table.find('id')
"""
@login_manager.request_loader
def load_user(request):
    db = dataset.connect('sqlite:///users.db')
    table=db['registered_users']
    token = request.headers.get('Authorization')
    if token is None:
        token = request.args.get('token')

    if token is not None:
        username,password = token.split(":") # naive token
        user_entry = table.find(username)
        
        if (user_entry is not None):
            user = User(user_entry['username'],user_entry['password'])
            if (user.password == password):
                return user
    return None
"""

# Load default config and override config from an environment variable
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'contacts.db'),
    DEBUG=True,
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)
def init_db():
    """This is to initialise database"""
    db = dataset.connect('sqlite:///contacts_up.db')
    table = db['person']

@app.cli.command('initdb')
def initdb_command():
    """Creates the database tables."""
    init_db()
    print('Initialized the database.')

@app.route('/')
def default_page():
    return render_template('index.html')

@app.route('/<username>')
def entry_page(username):
    if  session.get('logged_in'):
        db = dataset.connect('sqlite:///users.db')
        res=db['registered_users'].find(username)
        return render_template('index.html',res)
    return redirect(url_for(default_page))

@app.route('/added')
def show_entries():
    if not session.get('logged_in'):
        abort(401)
    """ This fetches the all entries and shows them all!! hurray !!!"""
    db = dataset.connect('sqlite:///contacts_up.db')
    result = db['person'].all()
    return render_template('show_entries.html', entries=result)

@app.route('/list')
#@login_required
def show_entry():
    if not session.get('logged_in'):
        abort(401)
    """ This fetches the all entries and shows them all!! hurray !!!"""
    db = dataset.connect('sqlite:///contacts_up.db')
    result = db['person'].all()
    return render_template('show_all.html', entries=result)

@app.route('/new', methods=['GET', 'POST'])
def add_new_contact():
    if session.get('logged_in'):
        return redirect(url_for('show_entries'))
    else:
        return render_template('login.html', error=error)

@app.route('/register', methods=['GET', 'POST'])
def login():
    """This is used to login to the app...fetches the username and passwd and shows them"""
    error = None
    db=dataset.connect('sqlite:///users.db')
    table=db['registered_users']
    result=table.all()
    if request.method == 'POST':
        for entry in result:
            if request.form['username'] == entry['username'] and (request.form['password'] == entry['password']):
                session['logged_in'] = True
                flash('You were logged in')
                break
    
    if session.get('logged_in'):
        return redirect(url_for('default_page'))
    else:
        return render_template('login.html', error=error)

@app.route('/add_new_login',methods=['POST'])
def add_new():
    """This is to add new registration"""
    db=dataset.connect('sqlite:///users.db')
    table=db['registered_users']
    result=table.all()
    flag=0
    for entry in result:
        if(request.form['username']==entry['username']):
            flag=1
            break
    if flag==1:
        #flash("username already exists")
        return render_template('index.html')
    else: 
        db['registered_users'].insert({
            "username" : request.form['username'],
            "password" : request.form['pwd'],
            "profile_pic" : request.form['pic']  
        })

        flash('new user is registered')
        return redirect(url_for('login'))

@app.route('/add', methods=['POST'])
#@login_required
def add_entry():
    """This is used to add entry in records..accessible only after logged in"""
    if not session.get('logged_in'):
        abort(401)
    db = dataset.connect('sqlite:///contacts_up.db')    
    db['person'].insert({
        "mobile": request.form['mobile'],
        "team_name": request.form['team_name'],
        "email": request.form['email'],
        "name": request.form['name']
    })
    r = redis.Redis(host='172.16.132.247', port=6379)
    user = {"mobile":request.form['mobile'], "team_name":request.form['team_name'], "email":request.form['email'], "name":request.form['name']}
    r.hmset("contacts", user)
    r.expire("contacts",60)
    ustring=request.form['mobile']+','+request.form['team_name']+','+request.form['email']+','+request.form['name']
    r.lpush("cons",ustring)
   # r.hmset("contacts",{request.form['name']:request.form['mobile']+','+request.form['team_name']+','+request.form['email']+','+request.form['name']})
    flash('New entry was successfully posted')
    return redirect(url_for('show_entries'))


@app.route('/user/<username>')
def show_user_profile(username):
    """show the user profile for that user"""
    if not session.get('logged_in'):
        abort(401)
    r = redis.Redis(host='172.16.132.247', port=6379)
    retlist=r.hgetall(username)
    if len(retlist) == 0:
        db = dataset.connect('sqlite:///contacts_up.db')    
        table = db['person']
        res = table.find(name = username)
        return render_template('show_user.html', entries = res)    
    else:
        return render_template('show_user.html', entries = retlist)

@app.route('/del/<username>')
def del_user_profile(username):
    """ delete the user profile for that user """
    if not session.get('logged_in'):
        abort(401)
    db = dataset.connect('sqlite:///contacts_up.db')    
    table = db['person']
    table.delete(name = username)
    
    res = table.all()
    return render_template('show_all.html', entries = res)

@app.route('/delete/<username>')
def delete_user_profile(username):
    """ delete the user profile for that user """
    if not session.get('logged_in'):
        abort(401)
    db = dataset.connect('sqlite:///contacts_up.db')    
    table = db['person']
    table.delete(name = username)
    
    res = table.all()
    return render_template('show_entries.html', entries = res) 

@app.route('/logout')
def logout():
    """log out please"""
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('login'))

if __name__ == '__main__':
    app.run(host = '127.0.0.1', port = 5000, debug = True)
